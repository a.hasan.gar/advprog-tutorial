package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employee;

public class SecurityExpert extends Employee {

    public SecurityExpert(String name, double salary) {
        if (salary < 70000.0) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.salary = salary;
        role = "Security Expert";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
