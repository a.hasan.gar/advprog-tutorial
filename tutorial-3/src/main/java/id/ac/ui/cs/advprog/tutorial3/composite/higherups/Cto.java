package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employee;

public class Cto extends Employee {

    public Cto(String name, double salary) {
        if (salary < 100000.0) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.salary = salary;
        role = "CTO";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
