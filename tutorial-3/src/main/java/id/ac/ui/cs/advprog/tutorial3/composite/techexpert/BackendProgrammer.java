package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employee;

public class BackendProgrammer extends Employee {

    public BackendProgrammer(String name, double salary) {
        if (salary < 20000.0) {
            throw new IllegalArgumentException();
        }

        this.name = name;
        this.salary = salary;
        role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
