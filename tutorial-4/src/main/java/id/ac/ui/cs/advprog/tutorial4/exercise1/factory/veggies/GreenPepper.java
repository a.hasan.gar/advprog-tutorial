package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class GreenPepper implements Veggies {
    public String toString() {
        return "Green Pepper";
    }
}
