package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class HardShellClams implements Clams {
    public String toString() {
        return "Hard-Shell Clams";
    }
}
