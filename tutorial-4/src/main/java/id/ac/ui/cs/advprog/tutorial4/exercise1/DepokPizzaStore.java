package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.DepokCheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.DepokSpecialSauceCheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class DepokPizzaStore extends PizzaStore {

    @Override
    protected Pizza createPizza(String type) {
        Pizza pizza = null;
        PizzaIngredientFactory pizzaIngredientFactory = new DepokPizzaIngredientFactory();

        if (type.equals("cheese")) {
            pizza = new DepokCheesePizza(pizzaIngredientFactory);
            pizza.setName("Depok Style Cheese Pizza");

        }
        else if (type.equals("special")) {
            pizza = new DepokSpecialSauceCheesePizza(pizzaIngredientFactory);
            pizza.setName("Depok Style Cheese Pizza w/ ABC Sauce");
        }

        return pizza;
    }
}
